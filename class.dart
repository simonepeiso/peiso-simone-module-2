import 'dart:core';

class WinningApp {
  String? appName;
  String? category;
  String? developer;
  int? year;

  WinningApp(String appName, String category, String developer, int year) {
    this.appName = appName.toUpperCase();
    this.category = category;
    this.developer = developer;
    this.year = year;
  }
}

void main() {
  var Winner2012 = WinningApp('FNB Banking', 'iOS', 'FNB', 2012);
  print(Winner2012.appName);
  print(Winner2012.category);
  print(Winner2012.developer);
  print(Winner2012.year);

  var Winner2013 = WinningApp('DStv', 'iOS', 'DStv', 2013);
  print(Winner2013.appName);
  print(Winner2013.category);
  print(Winner2013.developer);
  print(Winner2013.year);

  var Winner2014 = WinningApp('SyncMobile', 'iOS', 'iSync Solution', 2014);
  print(Winner2014.appName);
  print(Winner2014.category);
  print(Winner2014.developer);
  print(Winner2014.year);

  var Winner2015 = WinningApp('Takealot', 'Consumer', 'Takealot', 2015);
  print(Winner2015.appName);
  print(Winner2015.category);
  print(Winner2015.developer);
  print(Winner2015.year);

  var Winner2016 = WinningApp('Domestly', 'Consumer', 'Unknown', 2016);
  print(Winner2016.appName);
  print(Winner2016.category);
  print(Winner2016.developer);
  print(Winner2016.year);

  var Winner2017 = WinningApp('Hey Jude', 'Unknown', 'Unknown', 2017);
  print(Winner2017.appName);
  print(Winner2017.category);
  print(Winner2017.developer);
  print(Winner2017.year);

  var Winner2018 = WinningApp('Khula', 'Agriculture', 'Unknown', 2018);
  print(Winner2018.appName);
  print(Winner2018.category);
  print(Winner2018.developer);
  print(Winner2018.year);

  var Winner2019 = WinningApp('Over', 'Consumer', 'Unknown', 2019);
  print(Winner2019.appName);
  print(Winner2019.category);
  print(Winner2019.developer);
  print(Winner2019.year);

  var Winner2020 =
      WinningApp('Checkers Sixty60', 'Enterprise', 'Checkers', 2020);
  print(Winner2020.appName);
  print(Winner2020.category);
  print(Winner2020.developer);
  print(Winner2020.year);

  var Winner2021 =
      WinningApp('Ambani Africa', 'Educational', 'Ambani Africa', 2021);
  print(Winner2021.appName);
  print(Winner2021.category);
  print(Winner2021.developer);
  print(Winner2021.year);
}
