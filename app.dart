import 'dart:core';

void main() {
  List winners2012 = [
    'FNB Banking',
    'Health ID',
    'TransUnion',
    'Rapidtargets',
    'Matchy'
  ];
  List winners2013 = [
    'DStv',
    '.comm Telco',
    'MarkitShare',
    'NedBank App Suit',
    'Snap Scan',
    'Kids Aid',
    'Bookly',
    'Gautrain Buddy'
  ];
  List winners2014 = [
    'SyncMobile',
    'Supersport',
    'MyBelongings',
    'LIVE Inspect',
    'VIGO',
    'Zapper',
    'Rea Vaya',
    'Wildlife Tracker'
  ];
  List winners2015 = [
    'Takealot',
    'Standard Banks Shyft',
    'iiDENTIFii',
    'SiSa',
    'Guardian Health',
    'Murimi',
    'UniWise',
    'Kazi App',
    'STEM',
    'Afrihost',
    'Hellopay',
    'Road Save'
  ];
  List winners2016 = [
    'Domestly',
    'iKhokha',
    'HearZA',
    'Tuta-me',
    'KaChing',
    'Friendly Math Monsters for Kindergarten'
  ];
  List winners2017 = [
    'Hey Jade',
    'TransUnion 1Check',
    'EcoSlips',
    'InterGretMe',
    'Zilzi',
    'OrderIN',
    'STEM',
    'Pick n Pay Super Animals 2',
    'The TreeApp South Africa',
    'WatIf Health Portal',
    'Awethu Project',
    'Shyft for Standard Bank'
  ];
  List winners2018 = [
    'Khula',
    'Cowa Bunga',
    'Digemy Knowledge Partner and Besmarter',
    'Bestee',
    'ACGL',
    'dbTrac',
    'Stokfella',
    'Difela Hymns',
    'Xander English 1-20',
    'Ctrl',
    'Pineapple',
    'ASI Snakes'
  ];
  List winners2019 = [
    'SI Realities',
    'Lost Defence',
    'Franc',
    'Vula Mobile',
    'Matric Live',
    'STEM',
    'LocTransie',
    'Hydra',
    'Bottles',
    'Over',
    'Digger',
    'Mo Wash'
  ];
  List winners2020 = [
    'xamsta',
    'Checkers Sixty60',
    'BirdPro',
    'Lexie Hearing',
    'League of Legends',
    'GreenFingers Mobile',
    'Xitsonga Dictionary',
    'StokFella',
    'Bottles',
    'Guardian Health',
    'My Pregnancy Journey',
  ];
  List winners2021 = [
    'iiDENTIFii',
    'Hellopay ',
    'Guardian Health',
    'Ambani Africa',
    'Murimi',
    'Shyft for Standard Bank',
    'Sisa',
    'Kazi',
    'Takealot',
    'STEM',
    'Roadsave',
    'Afrihost'
  ];

  var appList = [
    winners2012,
    winners2013,
    winners2014,
    winners2015,
    winners2016,
    winners2017,
    winners2018,
    winners2019,
    winners2020,
    winners2021
  ].expand((x) => x).toList();

  appList.sort();
  var winner2017 = winners2017[0];
  var winner2018 = winners2018[0];
  int sum = appList.length;

  print('MTNapp of the year List:$appList');
  print('MTNapp of the year 2017 winner is:$winner2017');
  print('MTNapp of the year 2018 winner is:$winner2018');
  print("The total number of apps is:$sum");
}
