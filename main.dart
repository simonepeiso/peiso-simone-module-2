import 'dart:io';

void main() {
  print('Please enter your name: ');
  String? Name = stdin.readLineSync();

  print('Please enter your favourite app: ');
  String? FavouriteApp = stdin.readLineSync();

  print('Please enter your city: ');
  String? City = stdin.readLineSync();

  print(
      'Your name is $Name, You live in $City and Your favourite app is $FavouriteApp');
}
